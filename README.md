wdfs – Read-only Wikidata filesystem.

You can mount any Wikidata-Item to a folder. Items with P31 ("is a") relation to
the item will be displayed as files, P279 ("subclass of") are shown as subfolder.

Usage
-----
You can mount any item as normal user by:
```
$ python wdfs.py Q166142 /mountpoint
```
Unmount with fusermount:
```
$ fusermount -u /mountpoint
```

You can also install it to mount items the traditional way, using `mount` or `fstab`:
```
# install wdfs.py /usr/bin/wdfs
# mount Q166142 -t fuse.wdfs -o gid=100,uid=1000,allow_other /mountpoint
# umount /mountpoint
# cat > /etc/fstab
Q166142 /mountpoint fuse.wdfs allow_other,_netdev,users 0 0
```

To use the labels in some language instead of the QIDs as filenames, specify
the language with parameter `-o`:
```
$ python wdfs.py -o lang=en Q166142 /mountpoint
```

![](demo.gif)
![](caja.png)
![](dolphin.png)
