#!/usr/bin/env python

#    Copyright (C) 2018 Michael Schönitzer <michael@schoenitzer.de>
#
#    This program can be distributed under the terms of the GNU GPL.
#    See the file LICENSE.
#

import errno
import os
import pprint
import stat
import sys
from functools import lru_cache

import fuse
import requests
from fuse import Fuse

# pull in some spaghetti to make this stuff work without fuse-py being installed
try:
    import _find_fuse_parts
except ImportError:
    pass


if not hasattr(fuse, "__version__"):
    raise RuntimeError(
        "your fuse-py doesn't know of fuse.__version__, probably it's too old."
    )


cachesize = 512
pp = pprint.PrettyPrinter(indent=2)
fuse.fuse_python_api = (0, 2)


def ldfquery(subject, predicate, obj):
    url = "https://query.wikidata.org/bigdata/ldf"
    params = {"subject": subject, "predicate": predicate, "object": obj}
    headers = {"ACCEPT": "application/n-triples"}
    res = requests.get(url, params=params, headers=headers)
    if res.status_code != 200:
        return []
    lines = res.text.split("\n")
    lines = filter(lambda l: params["predicate"] in l, lines)
    entries = map(lambda l: l.split()[0][32:-1], lines)
    return {e: e for e in entries}


def spqlquery(predicate, obj, lang):
    url = "https://query.wikidata.org/sparql"
    query = """
select ?item ?label {{
  ?item wdt:{} wd:{}.
  ?item rdfs:label ?label. Filter( lang(?label) = "{}").
}} Limit 200
    """.format(
        predicate, obj, lang
    )
    params = {"query": query}
    headers = {"ACCEPT": "text/csv"}
    res = requests.get(url, params=params, headers=headers)
    if res.status_code != 200:
        return []
    res.encoding = "UTF-8"
    lines = res.text.split("\n")
    d = dict()
    for line in lines:
        elm = line.split(",")
        if len(elm) != 2:
            continue
        qid, name = elm
        if len(qid) < 31 or qid[31] != "Q":
            continue
        qid = qid[31:]
        name = name[0:-1].replace("/", "_")
        d[qid] = name
    return d


@lru_cache(maxsize=cachesize)
def getItems(parent, lang):
    if lang is None:
        return ldfquery(
            "",
            "http://www.wikidata.org/prop/direct/P31",
            "http://www.wikidata.org/entity/" + parent,
        )
    else:
        return spqlquery("P31", parent, lang)


@lru_cache(maxsize=cachesize)
def getSubItems(parent, lang):
    if lang is None:
        return ldfquery(
            "",
            "http://www.wikidata.org/prop/direct/P279",
            "http://www.wikidata.org/entity/" + parent,
        )
    else:
        return spqlquery("P279", parent, lang)


@lru_cache(maxsize=cachesize)
def getContent(qid):
    res = requests.get(
        "https://www.wikidata.org/wiki/Special:EntityData/{}.json".format(qid)
    )
    return res.json()


# This is not used since it is way to slow
# Instead we use 0 as filesize and enable direct_io
@lru_cache(maxsize=cachesize)
def getSize(qid):
    if qid[0] != "Q":
        return 0
    while True:
        res = requests.head(
            "https://www.wikidata.org/wiki/Special:EntityData/{}.json".format(qid)
        )
        if "Content-Length" in res.headers:
            return int(res.headers["Content-Length"])


class MyStat(fuse.Stat):
    def __init__(self):
        self.st_mode = 0
        self.st_ino = 0
        self.st_dev = 0
        self.st_nlink = 0
        self.st_uid = 0
        self.st_gid = 0
        self.st_size = 0
        self.st_atime = 0
        self.st_mtime = 0
        self.st_ctime = 0


class WikidataFS(Fuse):
    dirlist = {"/": -1}
    filelist = {}
    lang = None

    def __init__(self, tldqid, lang, *args, **kw):
        Fuse.__init__(self, *args, **kw)
        self.tldqid = tldqid
        self.lang = lang

    def getqid(self, path):
        if path[-1] == ".":
            path = path[:-1]
        if path == "/":
            return self.tldqid
        if path in self.dirlist:
            return self.dirlist[path]
        if path in self.filelist:
            return self.filelist[path]
        return None

    def getattr(self, path):
        st = MyStat()
        if path in self.dirlist:
            st.st_mode = stat.S_IFDIR | 0o755
            st.st_nlink = 2
        else:
            st.st_mode = stat.S_IFREG | 0o444
            st.st_nlink = 1
            st.st_size = 0
        return st

    def readdir(self, path, offset):
        qid = self.getqid(path)
        filelist = getItems(qid, self.lang)
        dirlist = getSubItems(qid, self.lang)
        self.dirlist.update(
            {os.path.join(path, name): qid for qid, name in dirlist.items()}
        )
        self.filelist.update(
            {os.path.join(path, name): qid for qid, name in filelist.items()}
        )
        for r in [".", "..", *dirlist.values(), *filelist.values()]:
            yield fuse.Direntry(r)

    def open(self, path, flags):
        if path not in self.filelist:
            return -errno.ENOENT
        accmode = os.O_RDONLY | os.O_WRONLY | os.O_RDWR
        if (flags & accmode) != os.O_RDONLY:
            return -errno.EACCES

    def read(self, path, size, offset):
        if path not in self.filelist:
            return -errno.ENOENT
        text = pp.pformat(getContent(self.getqid(path)))
        slen = len(text)
        if offset < slen:
            end = offset + size
            if end > slen:
                size = slen - offset
            buf = text[offset:end]
        else:
            buf = ""
        return buf


def main():
    usage = """
Userspace Wikidata Filesystem

wdfs.py Item [Options] Mountpoint/

Example:
    wdfs.py Q166142 Mountpoint/
"""

    if len(sys.argv) > 2:
        tldqid = sys.argv[1]
    else:
        tldqid = "Q14827288"

    lang = None
    # FIXME: Find out how to use python fuse-api to do this properly
    if "-o" not in sys.argv:
        sys.argv.append("-o")
        sys.argv.append("direct_io")
    else:
        idx = sys.argv.index("-o") + 1
        if "direct_io" not in sys.argv[idx]:
            sys.argv[idx] += ",direct_io"
        for arg in sys.argv[idx].split(","):
            if arg[:5] == "lang=":
                lang = arg[5:]
                sys.argv[idx] = sys.argv[idx].replace(arg, "")

    server = WikidataFS(
        tldqid=tldqid,
        lang=lang,
        version="%prog " + fuse.__version__,
        usage=usage,
        dash_s_do="setsingle",
    )

    server.parse(errex=1)
    server.main()


if __name__ == "__main__":
    main()
